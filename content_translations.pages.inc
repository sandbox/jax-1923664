<?php

function content_translations_overview_page() {
  $languages = language_list('enabled');
  $languages = $languages[1];
  $node_type_names = node_type_get_names();
  foreach ($node_type_names as $type => $name) {
    if (variable_get('language_content_type_' . $type) == TRANSLATION_ENABLED &&
        user_access("create $type content")) {
      $translated_types[$type] = $name;
    }
  }
  $types_to_filter = isset($_GET['types']) ?
    array_intersect(array_keys($translated_types), $_GET['types']) :
    array_keys($translated_types);
  if (isset($_GET['languages'])) {
    $languages = array_intersect_key($languages, array_flip($_GET['languages']));
  }
  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'type', 'tnid'))
    ->condition('n.type', $types_to_filter)
    ->condition(db_or()
        ->where('n.nid = n.tnid')
        ->condition('n.tnid', 0))
    ->extend('PagerDefault')
    ->limit(50);
  $count = array();
  foreach ($languages as $langcode => $language) {
    $alias = "node_" . str_replace('-', '_', $langcode);
    $query->leftJoin('node', $alias, "n.tnid = $alias.tnid AND $alias.language = '$langcode' AND ($alias.tnid != 0 OR n.nid = $alias.nid)");
    $query->addField($alias, 'title', "title_$langcode");
    $query->addField($alias, 'nid', "nid_$langcode");
    // TODO: Not supported on MSSQL, use ISNUMERIC. Find cross-db solution.
    $count[] = "!ISNULL($alias.nid)";
  }
  $query->addExpression(implode('+', $count), 'translation_count');
  if (!empty($_GET['limit-untranslated'])) {
    // TODO: Not supported on MSSQL.
    $query->having('translation_count < ' . count($languages));
  }
  $result = $query->execute();
  $langcodes = array_keys($languages);
  $rows = array();
  foreach ($result as $set) {
    $row = array();
    foreach ($langcodes as $langcode) {
      if (isset($set->{"nid_$langcode"})) {
        $content = l($set->{"title_$langcode"}, 'node/' . $set->{"nid_$langcode"}, array('language' => $languages[$langcode]));
      }
      else {
        $content = l(t('Add translation'), 'node/add/' . $set->type, array(
          'language' => $languages[$langcode],
          'query' => array(
            'translation' => $set->nid,
            'target' => $langcode,
          ),
          'attributes' => array('class' => array('add-translation')),
        ));
      }
      $row[$langcode] = $content;
    }
    $row['type'] = $node_type_names[$set->type];
    $rows[] = $row;
  }
  $form_state = array(
    'method' => 'get',
    'rerender' => TRUE,
    'no_redirect' => TRUE,
    'always_process' => TRUE,
    'build_info' => array('args' => array($translated_types)),
  );
  $form = drupal_build_form('content_translations_filter_options_form', $form_state, $translated_types);
  unset($form['form_build_id'], $form['form_token'], $form['form_id']);
  $content = array();
  $content['form'] = array('#markup' => drupal_render($form));
  $headers = array_map(function ($language)  { return $language->name; }, $languages);
  $headers[] = t('Type');
  $content['overview-table'] = array(
    '#header' => $headers,
    '#theme' => 'table',
    '#rows' => $rows,
  );
  $content['pager'] = array(
    '#theme' => 'pager',
  );
  return $content;
}

function content_translations_filter_options_form($form, $form_state, $types) {
  $form['#method'] = 'GET';
  $form['types'] = array(
    '#title' => t('Limit to types'),
    '#type' => 'checkboxes',
    '#options' => $types,
  );
  $form['languages'] = array(
    '#title' => t('Limit to languages'),
    '#type' => 'checkboxes',
    '#options' => locale_language_list(),
  );
  $form['actions'] = array('#type' => 'actions', '#weight' => 100);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['limit-untranslated'] = array(
    '#title' => t('Show only untranslated'),
    '#type' => 'checkbox',
    '#weight' => 110,
  );
  return $form;
}
